
WELCOME!

“Pearl” App Landing Page Template. Html5, CSS and Bootstrap v3.3.6
(written by Joel Wiesner <info (at) asoflatte (dot) com>)

------------------------------------------------------------------------

12-March-2015

template demo http://asoflatte.com/templates/pearl

------------------------------------------------------------------------

.:: INTRODUCTION ::.

Firstly, thank you for your support in downloading “Pearl”, a simple mobile app landing page template. This is a simple one page template designed for your app’s official website. 


.:: DEPENDENCIES ::.

- Bootstrap v3.3.6 (already added)
- jQuery v2.1.4 (already added)

------------------------------------------------------------------------

.:: FEATURES ::.

- Elegant single page design.
- Responsive layout for desktop AND mobile (mobile friendly)
- Parallax scrolling effects
- Smooth automatic scrolling on click events
- Carousel like slideshow of app screenshot images
- Social media integration in footer
- Font Awesome changeable icon font.
- Fade in effects on content throughout scroll 
- .PSD file of iPhone mock up included

------------------------------------------------------------------------

.:: DOCUMENTATION ::.

“Pearl” was designed to be simple and easy to get up and running.

Most of your changes (company name, logos, wording, headers etc) can be made in the index.html file.

Color changes on fonts and backgrounds and changing out background images can be changed in the styles.css file, found in the assets > css folder

ALL images are saved in the “images” folder.

The map on the contact section was embedded with Google Maps, see this link below for instructions on how to do this.
https://support.google.com/maps/answer/3544418?hl=en
(take out the width=“600px” height=“450px” in the <iframe> the map size is styled in the css)

------------------------------------------------------------------------

.:: CREDITS ::.

Bootstrap - http://getbootstrap.com
Stellar.js - http://markdalgleish.com/projects/stellar.js/
ScrollReveal.js - https://scrollrevealjs.org/
Slick.js - http://kenwheeler.github.io/slick/
Pexels - http://pexels.com
GraphicBurger - http://graphicburger.com/
Presskit() - http://dopresskit.com/
Google Fonts - http://google.com/fonts
MyFonts - http://myfonts.com
UIsurf “Nerdial App UI” - http://uisurf.com/downloads/nerdial-app-ui/

------------------------------------------------------------------------

.:: SUPPORT ::.

*Disclaimer*
We have not connected the contact page to an email address. 

If you come across any bugs, issues with the actual template or you simply want to leave a personal comment, you are welcome to write me.

Joel Wiesner
As Of Latte ®
www.AsOfLatte
