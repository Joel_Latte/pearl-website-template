$( document ).ready(function() {

	//Parallax Scrolling
		$.stellar({
			 horizontalOffset: 50,
			 verticalOffset: 50
		});


	// Smooth Scrolling
	    $('a[href*=#]:not([href=#])').click(function() {
	   	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			 if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
	         }, 1000 );
				return false;
	   	  }
			}
	    });


	//Slick Slider
		$('.screenshot-slider').slick({
		  centerMode: true,
		  centerPadding: '150px',
		  slidesToShow: 3,
		  arrows: true,
		  autoplay:true,
		  autoplaySpeed: 3000,
		  appendArrows: $("#arrow-spot"),
		  variableWidth: false,
		  responsive: [
		    {
		      breakpoint: 770,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '25px',
		        slidesToShow: 3
		      }
		    },
				{
		      breakpoint: 736,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '0px',
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '10px',
		        slidesToShow: 1
		      }
		    }
		  ]
		});


	// ScrollReveal
		window.sr = ScrollReveal();
		sr.reveal('.jumbo_container' , { duration: 2000, origin: 'bottom'});
		sr.reveal('.logo-feature'  , { duration: 1000, origin: 'top', distance: '100px' });
		sr.reveal('#about-text'  , { duration: 1500, origin: 'left', distance: '100px' });
		sr.reveal('#iPhoneRight'  , { duration: 1500, origin: 'right', distance: '100px' });
		sr.reveal('.screenshot-slider, #testimonials, #contactform, #location, #features'  , { duration: 1500, origin: 'bottom', distance: '100px'
		});


	// Fade  in Fade out
		$(function () {
	    var $element = $('.fa-angle-double-down');
	    setInterval(function () {
	        $element.fadeIn(250).delay(500).fadeOut(350).delay(500).fadeIn(350);
	    }, 350);
	});

});
